package com.example.rolldice

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import kotlin.random.Random

class MainActivity : AppCompatActivity() {

    lateinit var rollDiceButton: Button
    lateinit var diceImageView: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initialComponents()

        rollDiceButton.setOnClickListener {
            rolldice()
        }
    }

    private fun rolldice() {

        var random = Random.nextInt(6) + 1
        var drawableResource = when (random) {
            1 -> R.drawable.dice_1
            2 -> R.drawable.dice_2
            3 -> R.drawable.dice_3
            4 -> R.drawable.dice_4
            5 -> R.drawable.dice_5
            else -> R.drawable.dice_6
        }

        diceImageView.setImageResource(drawableResource)

    }

    private fun initialComponents() {
        rollDiceButton = findViewById(R.id.roll_button)
        diceImageView = findViewById(R.id.dice_image)
    }


}